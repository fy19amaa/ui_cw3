#include "mapcontainer.h"

MapContainer::MapContainer(QWidget *parent) : QWidget(parent)
{
    QImage image("../3840px-Blue_Marble_Padding.png");
    imageLabel->setPixmap(QPixmap::fromImage(image));

    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageLabel);
    scrollArea->setMinimumSize(500,500);
    QScrollBar* vScroll = scrollArea->verticalScrollBar();
    QScrollBar* hScroll = scrollArea->horizontalScrollBar();
    vScroll->setValue( (vScroll->maximum()+vScroll->minimum())/2 );
    hScroll->setValue( (hScroll->maximum()+hScroll->minimum())/2 );

    QImage imagePushPin("../ball_pushpin.png");
    ballPushPin->setPixmap(QPixmap::fromImage(imagePushPin.scaled(30, 30, Qt::KeepAspectRatio)));

    map_layout->addWidget(scrollArea,0,0,Qt::AlignCenter);
    map_layout->addWidget(ballPushPin,0,0,Qt::AlignCenter);
}

void MapContainer::setLocation(const double latitude, const double longitude) {
    double dx = (longitude/360)*map_width;
    double dy = (latitude/180)*map_height;
    double x0 = (double) scrollArea->horizontalScrollBar()->value();
    double y0 = (double) scrollArea->verticalScrollBar()->value();

    double x_error = 3*scrollArea->horizontalScrollBar()->singleStep();
    double y_error = 0;//-scrollArea->horizontalScrollBar()->singleStep();

    scrollArea->horizontalScrollBar()->setValue(x0+dx + x_error);
    scrollArea->verticalScrollBar()->setValue(y0-dy + y_error);
}
