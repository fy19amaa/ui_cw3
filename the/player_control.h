#ifndef PLAYER_CONTROL_H
#define PLAYER_CONTROL_H

#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include "the_player.h"
#include <vector>
#include <QTimer>



class PlayerControl  : public QPushButton {

Q_OBJECT

private:

public:

    // 0 = backwards button, 1 = forward button, 2 = play and pause button
    // 3 = previous button, 4 = next button, 5 = shuffle button
    PlayerControl(int mode, QWidget *parent) : QPushButton(parent){

        setIconSize(QSize(110,110));
        setMaximumWidth(110);

        if (mode == 1) {
            connect(this, SIGNAL(released()), this, SLOT (forw() ));
            setText("⏩ Forward");
        } else if (mode == 0) {
            connect(this, SIGNAL(released()), this, SLOT (backw() ));
            setText("⏪ Backward");
        } else if (mode == 2) {
            connect(this, SIGNAL(released()), this, SLOT (playy() ));
            setText("▶️/⏸");
        } else if (mode == 3) {
            connect(this, SIGNAL(released()), this, SLOT (playPrevious() ));
            setText("⏪ Previous");
        } else if (mode == 4) {
            connect(this, SIGNAL(released()), this, SLOT (playNext() ));
            setText("⏩ Next");
        } else if (mode == 5) {
            connect(this, SIGNAL(released()), this, SLOT (playRandom() ));
            setText("🔀 Shuffle");
        }

    }

private slots:

    void forw() {
        emit forwards();
    }

    void backw() {
        emit backwards();
    }

    void playy() {
        emit play1();
    }


    void playPrevious() {
        emit previous();
    }

    void playNext() {
        emit next();
    }

    void playRandom() {
        emit randomS();
    }


signals:

    void forwards();
    void backwards();
    void play1();
    void previous();
    void next();
    void randomS();

};






#endif // PLAYER_CONTROL_H
