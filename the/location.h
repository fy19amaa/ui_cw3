#ifndef LOCATION_H
#define LOCATION_H
#include <string>
#include <ctime>
using namespace std;
//notice the class is not completed yet, able to include new functions



class Location // location class include all location info
{
public:
    Location();
    string country; //here add country name
    string city; //here add city name
    float longitude; //here add longitude
    float latitude; //here add latitude
};

#endif // LOCATION_H
