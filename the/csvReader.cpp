#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "csvReader.h"

using namespace std;
vector<vector<string>> dataInCsv;

// read data from a file
// store the data in vector data list
void fileReader(string fileName) {
    ifstream file(fileName);
    if(!file.is_open()) cout << "can't read file" << endl;

    vector<string> elements;
    string line, element;

    while (getline(file, line)) {
        elements.clear();
        stringstream theline(line);
        while (getline(theline, element, ','))
        {
            elements.push_back(element);
        }
        dataInCsv.push_back(elements);
    }
}

// get data by video name and data type
// return 0 if data not found
string getData(string videoName, string dataType) {
    int data_index = 0;
    for (size_t i=0; i<dataInCsv[0].size(); ++i){
            if(dataInCsv[0][i] == dataType){
                data_index = i;
            }
    }
    for (size_t i=0; i<dataInCsv.size(); ++i){
        if(dataInCsv[i][0] == videoName){
            return dataInCsv[i][data_index];
        }
    }
    return 0;
}

string getTitle(string videoName) {
    return getData(videoName, "Title");
}

int getDuration(string videoName) {
    string duration = getData(videoName, "duration");
    int i=0;
    char c;
    string time;
    while(duration[i]){
        c=duration[i];
        time = time + c;
        if(isspace(c)){
            break;
        }
        i++;
    }
    return stoi(time);
}

string getCamera(string videoName) {
    return getData(videoName, "Camera");
}

bool isSlowMo(string videoName) {
    if (getData(videoName, "slowMo") == "yes"){
        return true;
    } else {
        return false;
    }
}

bool isSelfie(string videoName) {
    if (getData(videoName, "selfie") == "yes"){
        return true;
    } else {
        return false;
    }
}

string getDate(string videoName) {
    return getData(videoName, "date");
}

string getCountry(string videoName) {
    return getData(videoName, "country");
}

string getCity(string videoName) {
    return getData(videoName, "city");
}

double getLongitude(string videoName) {
    return stod(getData(videoName, "longitude"));
}

double getLatitude(string videoName) {
    return stod(getData(videoName, "latitude"));
}

string getDescription(string videoName) {
    return getData(videoName, "description");
}

int getViews(string videoName) {
    return stoi(getData(videoName, "views"));
}

int getLikes(string videoName) {
    return stoi(getData(videoName, "likes"));
}

string getActivity(string videoName) {
    return getData(videoName, "activity");
}
