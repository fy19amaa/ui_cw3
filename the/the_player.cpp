//
// Created by twak on 11/11/2019.
//

#include "the_player.h"
#include <iostream>
#include <QLabel>
#include <QVBoxLayout>
#include "video.h"
#include "mapcontainer.h"

std::string last5(std::string input) {
     return input.substr(input.size() - 5);
}

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}


// change the image and video of the buttons every one second forward
void ThePlayer::shuffle() {
    TheButtonInfo* i = & infos -> at (updateCount++ % infos->size() );
    buttons -> at(buttons->size() - 1) -> init(i);

    for (unsigned long long int u = (buttons->size() - 1), count = updateCount; u > 0 ; count++) {
        TheButtonInfo* ii = & infos -> at (count  % infos->size());
        buttons -> at(--u)  -> init(ii) ;
    }
}


// change the image and video of the buttons every one second backwards
void ThePlayer::shuffleBackwards() {
    updateCount--;
    TheButtonInfo* i = & infos -> at ((updateCount - 1) % infos->size() );
    buttons -> at(buttons->size() - 1) -> init(i);

    for (unsigned long long int u = (buttons->size() - 1), count = updateCount; u > 0 ; count++) {
        TheButtonInfo* ii = & infos -> at (count  % infos->size());
        buttons -> at(--u)  -> init(ii) ;
    }
}


void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}



void ThePlayer::jumpTo (TheButtonInfo* button) {
    currentFileName = last5(button->url->toString().toStdString());
    setMedia( * button -> url);
    play();
    playing = 1;
    indexOfCurrent(button);
}

string Bool2String(bool b) {
  return b ? "true" : "false";
}

void ThePlayer::setVideoDescription() {
    Video* currentVideo = new Video(this->currentFileName);
    QVBoxLayout* descLayout = (QVBoxLayout*) description->layout();

    QLayoutItem *wItem;
    while ((wItem = descLayout->takeAt(0)) != 0) {
        if (wItem->widget()) {
            wItem->widget()->setParent(NULL);
        }
        delete wItem;
    }

    MapContainer* mapContainer = new MapContainer();    mapContainer->setLocation(currentVideo->loc->latitude, currentVideo->loc->longitude);
    descLayout->addWidget(mapContainer);

    QLabel* title = new QLabel();
    title->setText(QString::fromStdString("Title: "+currentVideo->title));
    descLayout->addWidget(title);

    QLabel* loc = new QLabel();
    loc->setText(QString::fromStdString("Location: "+currentVideo->loc->country+", "+currentVideo->loc->city));
    descLayout->addWidget(loc);

    QLabel* date = new QLabel();
    date->setText(QString::fromStdString("Date: "+currentVideo->date));
    descLayout->addWidget(date);

    QLabel* desc = new QLabel();
    desc->setText(QString::fromStdString("Description: "+currentVideo->description));
    descLayout->addWidget(desc);

    QLabel* camera = new QLabel();
    camera->setText(QString::fromStdString("Camera: "+currentVideo->camera));
    descLayout->addWidget(camera);

    QLabel* slowMo = new QLabel();
    slowMo->setText(QString::fromStdString( "Slow Motion: "+Bool2String(currentVideo->slowMo) ) );
    descLayout->addWidget(slowMo);

    QLabel* selfie = new QLabel();
    selfie->setText(QString::fromStdString( "Selfie: "+Bool2String(currentVideo->selfie) ) );
    descLayout->addWidget(selfie);
}

// plays if paused, pause if playing
void ThePlayer::playOrPause() {

    if (playing == 1) {
        pause();
        playing = 0;
    } else {
        play();
        playing = 1;
    }
}


// returns the index of the video playing at the moment
void ThePlayer::indexOfCurrent(TheButtonInfo* button) {

    for (int i = 0; i < (int) infos->size(); i++) {
        if (infos->at(i).url == button -> url)
            current = i;
    }
}


// play the next video
void ThePlayer::nex() {

    int to = current - 1;

    if (to < 0)
        to = (int) infos->size() - 1;

    currentFileName = last5(infos->at(to).url->toString().toStdString());

    jumpTo(&infos->at(to));

    setVideoDescription();
}


// play the previous video
void ThePlayer::prev() {

    int to = current + 1;

    if (to >= (int) infos->size())
        to = 0;

    currentFileName = last5(infos->at(to).url->toString().toStdString());

    jumpTo(&infos->at(to));

    setVideoDescription();
}


// play the previous video
void ThePlayer::random() {

    int to = rand() % infos->size();

    currentFileName = last5(infos->at(to).url->toString().toStdString());

    jumpTo(&infos->at(to));

    setVideoDescription();
}
