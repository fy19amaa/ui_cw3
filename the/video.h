#ifndef VIDEO_H
#define VIDEO_H
#include <string>
#include "location.h"

using namespace std;
// notice the class is not completed yet, able to include new functions
// notice <ctime> library included

class Video
{
public:
    Video(string videoName);
    string title; //video title
    int duration; //video duration time
    string camera; // camera name
    bool slowMo; // slow motion
    bool selfie;
    string date;
    string description; //a short description for the video
    int views; // video views number
    int likes; // video likes number
    string activity;
    Location* loc;
};

#endif // VIDEO_H
