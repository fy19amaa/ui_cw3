//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>
#include <iostream>

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    std::vector<TheButtonInfo>* infos;
    std::vector<TheButton*>* buttons;
    QTimer* mTimer;
    long updateCount = 0;
    int playing = 1;
    int current = 0;

    void indexOfCurrent(TheButtonInfo* button);

public:
    ThePlayer() : QMediaPlayer(NULL) {
        setVolume(0); // be slightly less annoying
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)) );

        //mTimer = new QTimer(NULL);
        //mTimer->setInterval(1000); // 1000ms is one second between ...
        //mTimer->start();
        //connect( mTimer, SIGNAL (timeout()), SLOT ( shuffle() ) ); // ...running shuffle method
    }

    // all buttons have been setup, store pointers here
    void setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i);
    std::string currentFileName;
    QWidget* description;

private slots:

    // change the image and video of the buttons every one second forward
    void shuffle();

    // change the image and video of the buttons every one second backwards
    void shuffleBackwards();

    void playStateChanged (QMediaPlayer::State ms);

public slots:

    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);

    // plays if paused, pause if playing
    void playOrPause();

    // play the previous video
    void prev();

    // play the next video
    void nex();

    // play a random video
    void random();

    //set description
    void setVideoDescription();
};

#endif //CW2_THE_PLAYER_H
