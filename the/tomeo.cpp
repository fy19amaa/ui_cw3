/*    ______
//   /_  __/___  ____ ___  ___  ____
//    / / / __ \/ __ `__ \/ _ \/ __ \
//   / / / /_/ / / / / / /  __/ /_/ /
//  /_/  \____/_/ /_/ /_/\___/\____/
//              video for sports enthusiasts...
//
//  2811 cw3 : twak 11/11/2021
*/

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QTimer>
#include <string>
#include "the_player.h"
#include "the_button.h"
#include "player_control.h"
#include "moving_layout.h"
#include "video.h"
#include "mapcontainer.h"
#include "location.h"

#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif


std::string get_current_dir() {
   char buff[FILENAME_MAX]; //create string buffer to hold path
   GetCurrentDir( buff, FILENAME_MAX );
   string current_working_dir(buff);
   return current_working_dir;
}

Video* currentVideo = new Video("c.MOV");

// read in videos and thumbnails to this directory
std::vector<TheButtonInfo> getInfoIn (std::string loc) {

    std::vector<TheButtonInfo> out =  std::vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
#else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << endl;
        }
    }

    return out;
}

string BoolToString(bool b) {
  return b ? "true" : "false";
}

QWidget* createDescription(){
    QWidget* description = new QWidget();
    QVBoxLayout* descLayout = new QVBoxLayout();
    description->setLayout(descLayout);

    MapContainer* mapContainer = new MapContainer();
    mapContainer->setLocation(currentVideo->loc->latitude, currentVideo->loc->longitude);
    descLayout->addWidget(mapContainer);

    QLabel* title = new QLabel();
    title->setText(QString::fromStdString("Title: "+currentVideo->title));
    descLayout->addWidget(title);

    QLabel* loc = new QLabel();
    loc->setText(QString::fromStdString("Location: "+currentVideo->loc->country+", "+currentVideo->loc->city));
    descLayout->addWidget(loc);

    QLabel* date = new QLabel();
    date->setText(QString::fromStdString("Date: "+currentVideo->date));
    descLayout->addWidget(date);

    QLabel* desc = new QLabel();
    desc->setText(QString::fromStdString("Description: "+currentVideo->description));
    descLayout->addWidget(desc);

    QLabel* camera = new QLabel();
    camera->setText(QString::fromStdString("Camera: "+currentVideo->camera));
    descLayout->addWidget(camera);

    QLabel* slowMo = new QLabel();
    slowMo->setText(QString::fromStdString( "Slow Motion: "+BoolToString(currentVideo->slowMo) ) );
    descLayout->addWidget(slowMo);

    QLabel* selfie = new QLabel();
    selfie->setText(QString::fromStdString( "Selfie: "+BoolToString(currentVideo->selfie) ) );
    descLayout->addWidget(selfie);

    return description;
}

int main(int argc, char *argv[]) {

    cout << get_current_dir() << endl;

    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << endl;

    // create the Qt Application and window
    QApplication app(argc, argv);
    QWidget window;

    // collect all the videos in the folder
    std::vector<TheButtonInfo> videos;

    if (argc == 2)
        videos = getInfoIn( std::string(argv[1]) );

    if (videos.size() == 0) {

        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );

        switch( result )
        {
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }
        exit(-1);
    }



    // the number of buttons to be used
    int numberOfButtons = 4;


    // the widget that will show the video
    QVideoWidget *videoWidget = new QVideoWidget;

    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);

    // a row of buttons
    QWidget *buttonWidget = new QWidget();

    // a row of control buttons
    QWidget *buttonWidgetControl= new QWidget();

    // a list of the buttons
    std::vector<TheButton*> buttons;

    // the buttons are arranged horizontally
    MovingLayout *layout = new MovingLayout(150, 5000, 1);
    buttonWidget->setLayout(layout);
    layout->connect(layout, SIGNAL(fullRotation()), player, SLOT (shuffle()));

    // the playback control buttons  arranged horizontally
    QHBoxLayout *layoutControl = new QHBoxLayout();
    buttonWidgetControl->setLayout(layoutControl);

    // volume slider
    QSlider* volumeSlider = new QSlider(Qt::Horizontal);
    volumeSlider->connect(volumeSlider, SIGNAL(valueChanged(int)), player, SLOT (setVolume(int)));
    layoutControl->addWidget(volumeSlider);
    volumeSlider->setMaximumWidth(100);
    volumeSlider->setMinimumWidth(50);

    // previous button
    PlayerControl* previousButton = new PlayerControl(3, buttonWidgetControl);
    previousButton->connect(previousButton, SIGNAL(previous()), player, SLOT (prev()));
    layoutControl->addWidget(previousButton);

    // play and pause button
    PlayerControl* playButton = new PlayerControl(2, buttonWidgetControl);
    playButton->connect(playButton, SIGNAL(play1()), player, SLOT (playOrPause()));
    layoutControl->addWidget(playButton);

    // next button
    PlayerControl* nextButton = new PlayerControl(4, buttonWidgetControl);
    nextButton->connect(nextButton, SIGNAL(next()), player, SLOT (nex()));
    layoutControl->addWidget(nextButton);

    // shuffle button
    PlayerControl* shuffleButton = new PlayerControl(5, buttonWidgetControl);
    shuffleButton->connect(shuffleButton, SIGNAL(randomS()), player, SLOT (random()));
    layoutControl->addWidget(shuffleButton);

    // forward and backwards buttons (need to be initialized after the buttons unlike the rest
    PlayerControl* forwardButton = new PlayerControl(1, buttonWidget);
    forwardButton->connect(forwardButton, SIGNAL(forwards()), player, SLOT (shuffle()));
    PlayerControl* backwardButton = new PlayerControl(0, buttonWidget);
    backwardButton->connect(backwardButton, SIGNAL(backwards()), player, SLOT (shuffleBackwards()));

    layout->addWidget(backwardButton); // adding the backwards button

    // create the four buttons
    for ( int i = 0; i < numberOfButtons; i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo*)), player, SLOT (jumpTo(TheButtonInfo*)) ); // when clicked, tell the player to play.
        button->connect(button, SIGNAL(clicked()), player, SLOT (setVideoDescription()) );
        buttons.push_back(button);
        layout->addWidget(button);
        button->init(&videos.at(i));
    }

    layout->addWidget(forwardButton); // adding the forwards button

    // tell the player what buttons and videos are available
    player->setContent(&buttons, & videos);

    // create the main window and layout
    QVBoxLayout *top = new QVBoxLayout();

    //create mainLayout
    QHBoxLayout* mainLayout = new QHBoxLayout();
    QWidget* playerWidget = new QWidget();
    playerWidget->setLayout(top);
    mainLayout->addWidget(playerWidget);

    QWidget* description = createDescription();
    player->description = description;
    //description->setStyleSheet("background-color: white");
    mainLayout->addWidget(description);

    window.setLayout(mainLayout);



    window.setWindowTitle("tomeo");
    window.setMinimumSize(800, 680);

    // add the video and the buttons to the vertical widget
    top->addWidget(videoWidget);
    top->addWidget(buttonWidgetControl);
    top->addWidget(buttonWidget);

    // showtime!
    window.show();

    // wait for the app to terminate
    return app.exec();
}
