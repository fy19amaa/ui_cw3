#ifndef MAPCONTAINER_H
#define MAPCONTAINER_H

#include <QLabel>
#include <QScrollArea>
#include <QScrollBar>
#include <QGridLayout>
#include <QWidget>

class MapContainer : public QWidget
{
    Q_OBJECT
public:
    explicit MapContainer(QWidget *parent = nullptr);
    void setLocation(const double latitude,const double longitude);

private:
    double map_width = 3840;
    double map_height = 1920;
    QGridLayout* map_layout = new QGridLayout(this);
    QLabel *imageLabel = new QLabel;
    QScrollArea* scrollArea = new QScrollArea;
    QLabel* ballPushPin = new QLabel;

signals:

};

#endif // MAPCONTAINER_H
