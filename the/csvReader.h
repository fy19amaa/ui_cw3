#ifndef CSVREADER_H
#define CSVREADER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

void fileReader(string fileName);
string getData(string videoName, string dataType);
string getTitle(string videoName);
int getDuration(string videoName);
string getCamera(string videoName);
bool isSlowMo(string videoName);
bool isSelfie(string videoName);
string getDate(string videoName);
string getCountry(string videoName);
string getCity(string videoName);
double getLongitude(string videoName);
double getLatitude(string videoName);
string getDescription(string videoName);
int getViews(string videoName);
int getLikes(string videoName);
string getActivity(string videoName);

#endif // CSVREADER_H
