#include "video.h"
#include "csvReader.h"

Video::Video(string videoFileName) {

    string csvFileName = "../videosCsv.csv";
    fileReader(csvFileName);

    this->title = getTitle(videoFileName);
    this->duration = getDuration(videoFileName);
    this->camera = getCamera(videoFileName);
    this->slowMo = isSlowMo(videoFileName);
    this->selfie = isSelfie(videoFileName);
    this->date = getDate(videoFileName);
    this->description = getDescription(videoFileName);
    this->views = getViews(videoFileName);
    this->likes = getLikes(videoFileName);
    this->activity = getActivity(videoFileName);
    this->loc = new Location();
    this->loc->city = getCity(videoFileName);
    this->loc->country = getCountry(videoFileName);
    this->loc->latitude = getLatitude(videoFileName);
    this->loc->longitude = getLongitude(videoFileName);

}
