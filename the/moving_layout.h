#ifndef MOVING_LAYOUT_H
#define MOVING_LAYOUT_H


#include <QtWidgets>
#include <QRect>
#include <QTimer>




class MovingLayout : public QHBoxLayout {
    Q_OBJECT


private:

    int timeInterval;
    int fps;
    float blockSize;
    float leftMargin;
    float rightMargin;
    QTimer* mTimer;

public:

    MovingLayout(int buttonSize, int intervalPeriod, int fpss) {

        fps = fpss;
        timeInterval = intervalPeriod;
        blockSize = buttonSize;
        leftMargin = 0;
        rightMargin = blockSize - leftMargin;

        // moving the layout to the right, to create the illusion of the boxes moving smoothely
        //this->setContentsMargins(leftMargin,0,rightMargin,0); // (left, top, right, button)


        mTimer = new QTimer(NULL);
        mTimer->setInterval(timeInterval /(fps)); // frames per second ...
        mTimer->start();

        if (fps == 1) {
            connect( mTimer, SIGNAL (timeout()), SLOT ( move() ) ); // ...running shuffle method
        } else {
            connect( mTimer, SIGNAL (timeout()), SLOT ( smoothMove() ) ); // ...running shuffle method
        }

    }



public slots:

    void smoothMove() {

        if (leftMargin < blockSize) {
            leftMargin = leftMargin + (blockSize / (fps));
            rightMargin = blockSize - leftMargin;
            this->setContentsMargins((int) leftMargin, 0 , (int) rightMargin, 0); // (left, top, right, button)
        } else {
            leftMargin = 0;
            rightMargin = blockSize - leftMargin;
            this->setContentsMargins((int) leftMargin, 0 , (int) rightMargin, 0); // (left, top, right, button)

            emit fullRotation();
        }
    }

    void move() {
            emit fullRotation();
    }


signals:

    void fullRotation();


};








#endif // MOVING_LAYOUT_H
